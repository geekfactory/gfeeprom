# GFEEPROM Library #

Alternative library to read / write internal EEPROM on AVR architectures, the library is designed to be compatible with the original EEPROM library shipped with the IDE. The library supports reading and writing different data types, such as strings, floats, longs, etc. Other important feature added with this library is the integrity check of the EEPROM contents.

## Supported devices ##

This library was developed/tested on the following boards:

* Arduino UNO R3
* Arduino Mega 2560 R3

## Contact me ##

* Feel free to write for any inquiry: ruben at geekfactory.mx
* Check our website: https://www.geekfactory.mx



