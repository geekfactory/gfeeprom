#######################################
# Syntax Coloring Map for GFEEPROM
#######################################

#######################################
# Datatypes (KEYWORD1)
#######################################
GFEEPROM			KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################
begin				KEYWORD2
check				KEYWORD2
computeChecksum		KEYWORD2
saveChecksum		KEYWORD2
readBit				KEYWORD2
checkBits			KEYWORD2
readByte			KEYWORD2
readWord			KEYWORD2
readDword			KEYWORD2
readFloat			KEYWORD2
readBlock			KEYWORD2
readString			KEYWORD2
print				KEYWORD2
writeBit			KEYWORD2
setBits				KEYWORD2
clearBits			KEYWORD2
writeByte			KEYWORD2
writeWord			KEYWORD2
writeDword			KEYWORD2
writeFloat			KEYWORD2
writeBlock			KEYWORD2
writeString			KEYWORD2
clear				KEYWORD2
updateByte			KEYWORD2
updateWord			KEYWORD2
updateDword			KEYWORD2
updateFloat			KEYWORD2
updateBlock			KEYWORD2
read				KEYWORD2
write				KEYWORD2
update				KEYWORD2

#######################################
# Instances (KEYWORD2)
#######################################
EEPROM				KEYWORD2

#######################################
# Constants (LITERAL1)
#######################################
E_EEPROM_OK					LITERAL1
E_EEPROM_INVALID_SIGNATURE	LITERAL1
E_EEPROM_INVALID_VERSION	LITERAL1
E_EEPROM_INVALID_CHECKSUM	LITERAL1
E_EEPROM_IO_ERROR			LITERAL1