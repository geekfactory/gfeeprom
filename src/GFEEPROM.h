/*	GeekFactory GFEEPROM library
 
	Copyright (C) 2017 Jesus Ruben Santa Anna Zamudio.
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
 
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
	Author website: http://www.geekfactory.mx
	Author e-mail: ruben at geekfactory dot mx
 */

#ifndef GFEEPROM_H
#define GFEEPROM_H

/*-------------------------------------------------------------*/
/*		Includes and dependencies			*/
/*-------------------------------------------------------------*/
#include <Arduino.h> 
#include <stdint.h>
#include <avr/eeprom.h>

/*-------------------------------------------------------------*/
/*		Library configuration				*/
/*-------------------------------------------------------------*/
#define GFEEPROM_VERSION_STRING     "1.0.1"

/*-------------------------------------------------------------*/
/*		Macros and definitions				*/
/*-------------------------------------------------------------*/

/*-------------------------------------------------------------*/
/*		Typedefs enums & structs			*/
/*-------------------------------------------------------------*/
/**
 * This enumeration defines the EEPROM data integrity check results
 */
enum eeprom_results {
	E_EEPROM_OK = 0,
	E_EEPROM_INVALID_SIGNATURE,
	E_EEPROM_INVALID_VERSION,
	E_EEPROM_INVALID_CHECKSUM,
	E_EEPROM_IO_ERROR,
};

/**
 * Represents the appended data block for integrity check
 */
struct eepromcheck {
	uint8_t signature;
	uint8_t version;
	uint16_t checksum;
};

/*-------------------------------------------------------------*/
/*			Class declaration			*/
/*-------------------------------------------------------------*/
class GFEEPROM {
public:
	/**
	 * Class constructor (empty)
	 */
	GFEEPROM();

	/*-------------------------------------------------------------*/
	/*		EEPROM Content Validation API			*/
	/*-------------------------------------------------------------*/
	/**
	 * Initializes the EEPROM library for it´s use
	 * 
	 * Initialize the managed EEPROM addresses and prepare the library for
	 * use.
	 * 
	 * @param start Start address of the managed EEPROM
	 * @param size The size of the managed EEPROM memory
	 * @param version The version ID for the software that is storing
	 * information on the EEPROM memory.
	 */
	void begin(uint16_t start = 0x0000, uint16_t size = 0x0000, uint8_t version = 0x01);

	/**
	 * Checks the integrity of the EEPROM contents and returns a value
	 * indicating if the EEPROM is correct or if any of the performed tests
	 * fails.
	 * 
	 * @returns Returns E_EEPROM_OK (0) if memory passes integrity check.
	 * Returns any of the values defined in enum eeprom_results.
	 */
	enum eeprom_results check();

	/**
	 * Computes and returns the verification sum value for the EEPROM
	 * contents.
	 * 
	 * @returns EEPROM verification value 
	 */
	uint16_t computeChecksum();

	/**
	 * Computes the checksum value for the current EEPROM contents and
	 * stores it on the appropriate location for validation.
	 */
	void saveChecksum();

	/*-------------------------------------------------------------*/
	/*			Read API				*/
	/*-------------------------------------------------------------*/
	/**
	 * Reads a single bit from EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param bit The bit number to check
	 * 
	 * @returns Returns true if the bit is set, false otherwise
	 */
	bool readBit(uint16_t addr, uint8_t bit);

	/**
	 * Checks if any bit on a given EEPROM location matches with the given
	 * bitmask (performs bitwise AND operation of EEPROM contents with the
	 * given bitmask.
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param mask A bitmask containing the bits to check for match
	 * 
	 * @returns Returns true if any bit is set in both: The EEPROM memory
	 * and the given bitmask. False if no matching bit is set.
	 */
	bool checkBits(uint16_t addr, uint8_t mask);

	/**
	 * Reads a byte from EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * 
	 * @returns A byte read from EEPROM
	 */
	uint8_t readByte(uint16_t addr);

	/**
	 * Reads an integer from EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * 
	 * @returns A 16 bit integer
	 */
	uint16_t readWord(uint16_t addr);

	/**
	 * Reads a long integer from EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * 
	 * @returns A 32 bit integer
	 */
	uint32_t readDword(uint16_t addr);

	/**
	 * Reads a floating point number from EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * 
	 * @returns A floating point number as read from EEPROM
	 */
	float readFloat(uint16_t addr);

	/**
	 * Reads a block of data from EEPROM memory
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param dst The pointer to the buffer where the data is copied
	 * @param len The amount of data bytes to copy
	 */
	void readBlock(uint16_t addr, void * dst, uint16_t len);

	/**
	 * Reads a null terminated string from EEPROM to RAM buffer
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param buf The buffer where the string is to be copied
	 * @param len The maximum amount of data to copy
	 * 
	 * @returns If the complete string can be read from EEPROM to the
	 * provided buffer, return true: If buffer cannot accommodate the string
	 * return false.
	 */
	bool readString(uint16_t addr, char * buf, uint8_t len);

	/**
	 * Prints a string stored in EEPROM to the given stream
	 * 
	 * @param addr The address where the string is stored
	 * @param max The maximum length of the string to print
	 * @param stream The stream to write the characters to
	 */
	void print(uint16_t addr, uint16_t max, Stream & stream);

	/*-------------------------------------------------------------*/
	/*			Write API				*/
	/*-------------------------------------------------------------*/
	/**
	 * Writes a single bit to EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param bit The bit number to write
	 * @param val The value to write (1 or 0, HIGH or LOW)
	 * 
	 * @returns Returns true if operation is successful
	 */
	void writeBit(uint16_t addr, uint8_t bit, bool val);

	/**
	 * Sets the indicated bits on EEPROM location according to a bitmask
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param mask The bitmask containing the bits to set
	 */
	void setBits(uint16_t addr, uint8_t mask);

	/**
	 * Clears the indicated bits on EEPROM location according to a bitmask
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param mask The bitmask containing the bits to clear
	 */
	void clearBits(uint16_t addr, uint8_t mask);

	/**
	 * Writes a byte to EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param val The value to write to EEPROM
	 */
	void writeByte(uint16_t addr, uint8_t val);

	/**
	 * Writes an integer to EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param val The value to write to EEPROM
	 */
	void writeWord(uint16_t addr, uint16_t val);

	/**
	 * Writes a long integer to EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param val The value to write to EEPROM
	 */
	void writeDword(uint16_t addr, uint32_t val);

	/**
	 * Writes a floating point number to EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param val The value to write to EEPROM
	 */
	void writeFloat(uint16_t addr, float val);

	/**
	 * Writes a block of data to the EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param src The pointer to the buffer containing data to write
	 * @param len The length of the data to write
	 */
	void writeBlock(uint16_t addr, const void * src, uint16_t len);

	/**
	 * Writes a string to EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param str Buffer containing a string to be written
	 * @param len The maximum EEPROM space that the string can use
	 * 
	 * @returns True if the full string can be stored, false if string cannot
	 * fit in the assigned storage.
	 */
	bool writeString(uint16_t addr, const char * str, uint8_t len);

	/**
	 * Clears the EEPROM and fills it with the value passed as a parameter.
	 * If managed memory is enabled, this also computes the value for
	 * the empty memory and places it
	 * 
	 * @param value The default (blank) value to write to the EEPROM.
	 */
	void clear(uint8_t value = 0x00);

	/*-------------------------------------------------------------*/
	/*			Update API				*/
	/*-------------------------------------------------------------*/
	/**
	 * Updates a single byte on EEPROM. This method only writes to EEPROM
	 * if the value passed is different than the one stored on the given
	 * location.
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param val The value to write to EEPROM
	 */
	void updateByte(uint16_t addr, uint8_t val);

	/**
	 * Updates an integer on EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param val The value to write to EEPROM
	 */
	void updateWord(uint16_t addr, uint16_t val);

	/**
	 * Updates a long integer on EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param val The value to write to EEPROM
	 */
	void updateDword(uint16_t addr, uint32_t val);

	/**
	 * Updates a floating point number on EEPROM
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param val The value to write to EEPROM
	 */
	void updateFloat(uint16_t addr, float val);

	/**
	 * Updates a block of EEPROM memory
	 * 
	 * @param addr The EEPROM address where data is stored
	 * @param val The value to write to EEPROM
	 */
	void updateBlock(uint16_t addr, const void * buf, uint16_t len);

	/*-------------------------------------------------------------*/
	/*	For compatibility with Arduino EEPROM library		*/
	/*-------------------------------------------------------------*/
	inline uint8_t read(uint16_t addr)
	{
		return readByte(addr);
	};

	inline void write(uint16_t addr, uint8_t val)
	{
		writeByte(addr, val);
	};
	
	inline void update(uint16_t addr, uint8_t val)
	{
		updateByte(addr, val);
	};
public:
	uint16_t _start;
	uint16_t _size;
	uint16_t _version;
	uint8_t _errorCode;
};

extern GFEEPROM EEPROM;

#endif
