/*	GeekFactory GFEEPROM library
 
	Copyright (C) 2017 Jesus Ruben Santa Anna Zamudio.
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
 
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
	Author website: http://www.geekfactory.mx
	Author e-mail: ruben at geekfactory dot mx
 */

#include "GFEEPROM.h"

GFEEPROM::GFEEPROM()
{
}

void GFEEPROM::begin(uint16_t start, uint16_t size, uint8_t version)
{
	_start = start;
	_size = size;
	_version = version;
}

enum eeprom_results GFEEPROM::check()
{
	struct eepromcheck verification;

	// Get verification block from EEPROM
	readBlock(_start + _size, & verification, sizeof(struct eepromcheck));

	// Check signature value
	if (verification.signature != 0xAA)
		return E_EEPROM_INVALID_SIGNATURE;

	// Check EEPROM version number
	if (verification.version != _version)
		return E_EEPROM_INVALID_VERSION;

	// Perform checksum validation
	if (verification.checksum != computeChecksum())
		return E_EEPROM_INVALID_CHECKSUM;

	return E_EEPROM_OK;
}

uint16_t GFEEPROM::computeChecksum()
{
	uint16_t i;
	uint16_t sum = 0;
	for (i = _start; i < _start + _size; i++)
		sum += readByte(i);

	return 0xBABA - sum;
}

void GFEEPROM::saveChecksum()
{
	struct eepromcheck verification;

	if (_size != 0) {

		// Initialize EEPROM verification block

		verification.signature = 0xAA;
		verification.version = _version;
		verification.checksum = computeChecksum();
		// Write 
		eeprom_update_block(&verification, (void *) (_start + _size), sizeof(struct eepromcheck));
	}
}

bool GFEEPROM::readBit(uint16_t addr, uint8_t bit)
{
	return readByte(addr) & (1 << bit);
}

bool GFEEPROM::checkBits(uint16_t addr, uint8_t mask)
{
	return((readByte(addr) & mask) == 0) ? false : true;
}

uint8_t GFEEPROM::readByte(uint16_t addr)
{
	return eeprom_read_byte((uint8_t *) addr);
}

uint16_t GFEEPROM::readWord(uint16_t addr)
{
	return eeprom_read_word((uint16_t *) addr);
}

uint32_t GFEEPROM::readDword(uint16_t addr)
{
	return eeprom_read_dword((uint32_t *) addr);
}

float GFEEPROM::readFloat(uint16_t addr)
{
	return eeprom_read_float((float *) addr);
}

void GFEEPROM::readBlock(uint16_t addr, void * dst, uint16_t len)
{

	eeprom_read_block(dst, (void *) addr, len);
}

bool GFEEPROM::readString(uint16_t addr, char * buf, uint8_t len)
{
	uint16_t index = 0;
	do {
		buf[index] = readByte(addr + index);
	} while (buf[index++] && index < len);

	// If the complete string can be read from EEPROM to the provided buffer
	// return true, if buffer cannot accommodate the string, return false.

	return(index < len) ? true : false;
}

void GFEEPROM::print(uint16_t addr, uint16_t max, Stream & stream)
{
	char c;
	uint16_t count = 0;

	do {
		c = readByte(addr++);
		count++;
		stream.write(c);
	} while (c && count < max);
}

void GFEEPROM::writeBit(uint16_t addr, uint8_t bit, bool val)
{
	// Check if valid bit
	if (bit > 7)
		return false;
	// Read Actual value
	uint8_t out = readByte(addr);
	// Set / clear desired bit
	if (val) {
		out |= (1 << bit); //Set bit to 1
	} else {
		out &= ~(1 << bit); //Set bit to 0
	}
	// Store if different from input
	updateByte(addr, out);
	saveChecksum();
	return true;
}

void GFEEPROM::setBits(uint16_t addr, uint8_t mask)
{
	updateByte(addr, readByte(addr) | mask);
	saveChecksum();
}

void GFEEPROM::clearBits(uint16_t addr, uint8_t mask)
{
	updateByte(addr, readByte(addr) & (~mask));
	saveChecksum();
}

void GFEEPROM::writeByte(uint16_t addr, uint8_t val)
{
	eeprom_write_byte((uint8_t *) addr, val);
	saveChecksum();
}

void GFEEPROM::writeWord(uint16_t addr, uint16_t val)
{

	eeprom_write_word((uint16_t *) addr, val);
	saveChecksum();
}

void GFEEPROM::writeDword(uint16_t addr, uint32_t val)
{
	eeprom_write_dword((uint32_t *) addr, val);
	saveChecksum();
}

void GFEEPROM::writeFloat(uint16_t addr, float val)
{
	eeprom_write_float((float *) addr, val);
	saveChecksum();
}

void GFEEPROM::writeBlock(uint16_t addr, const void * src, uint16_t len)
{
	eeprom_write_block(src, (void *) addr, len);
	saveChecksum();
}

bool GFEEPROM::writeString(uint16_t addr, const char * str, uint8_t len)
{
	uint8_t i;
	uint16_t l = strlen(str);
	// Check that string can fit inside the indicated EEPROM size
	if (l >= len)
		return false;
	// Write array to eeprom
	for (i = 0; i < l; i++)
		writeByte(addr++, str[i]);
	// Write string terminator at the end
	EEPROM.write(addr, 0x00);
	saveChecksum();
	return true;
}

void GFEEPROM::clear(uint8_t value)
{
	uint16_t i;
	struct eepromcheck verification;

	// Initialize EEPROM verification block
	verification.signature = 0xAA;
	verification.version = _version;
	verification.checksum = 0;
	// Clear all memory addresses to the indicated value
	for (i = _start; i < _start + _size; i++) {
		verification.checksum += value;
		updateByte(i, value);
	}
	// No Supervised EEPROM
	if (_size == 0)
		return;
	// Final checksum calculation
	verification.checksum = 0xBABA - verification.checksum;
	// Write verification at the end of managed EEPROM
	writeBlock(_start + _size, &verification, sizeof(struct eepromcheck));
}

void GFEEPROM::updateByte(uint16_t addr, uint8_t val)
{
	eeprom_update_byte((uint8_t *) addr, val);
	saveChecksum();
}

void GFEEPROM::updateWord(uint16_t addr, uint16_t val)
{
	eeprom_update_word((uint16_t *) addr, val);
	saveChecksum();
}

void GFEEPROM::updateDword(uint16_t addr, uint32_t val)
{
	eeprom_update_dword((uint32_t *) addr, val);
	saveChecksum();
}

void GFEEPROM::updateFloat(uint16_t addr, float val)
{
	eeprom_update_float((float *) addr, val);
	saveChecksum();
}

void GFEEPROM::updateBlock(uint16_t addr, const void * buf, uint16_t len)
{
	eeprom_update_block(buf, (void *) addr, len);
	saveChecksum();
}

GFEEPROM EEPROM;
